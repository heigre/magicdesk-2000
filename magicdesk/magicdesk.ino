#define trigPin 3
#define echoPin 2
#define led 11
#define raisePin 4
#define lowerPin 5

// Define the digital power pins
#define pwr_6 6
#define pwr_7 7
#define pwr_8 8

// Define the analog sensor pins
#define switch_increase A0
#define switch_decrease A1
#define switch_enable A2

// Constants for the heights and timing
const int highHeight = 90; // High height in cm
const int lowHeight = 61; // Low height in cm
int tempHeight = 61;
const unsigned long switchInterval = 20000; // 20 minutes in milliseconds
const int hysteresis = 2; // Hysteresis in cm to prevent noise

// Variables to manage the desk state and timing
unsigned long previousMillis = 0;
bool deskRaised = false;

void setup() {
    Serial.begin(9600);
    pinMode(trigPin, OUTPUT);
    pinMode(echoPin, INPUT);
    pinMode(led, OUTPUT);
    pinMode(raisePin, OUTPUT);
    pinMode(lowerPin, OUTPUT);
    pinMode(pwr_6, OUTPUT);
    pinMode(pwr_7, OUTPUT);
    pinMode(pwr_8, OUTPUT);
}

void loop() {
    unsigned long currentMillis = millis();
    
    // Read state of the switches
    int enable = analogRead(switch_enable);

    Serial.print("Enable: ");
    Serial.print(enable);

    if (enable > 100){
        Serial.print("Auto mode");
        // Check if it's time to switch the desk state
        if (currentMillis - previousMillis >= switchInterval) {
            deskRaised = !deskRaised; // Toggle the state
            previousMillis = currentMillis; // Save the switch time
        }
        int desiredHeight = deskRaised ? highHeight : lowHeight;
        controlDesk(desiredHeight);
    } else {
        tempHeight = measureDistance();
        while (enable < 100) {
            Serial.print("Manual mode");
            // Use the switches to control the desk
            // Set height to current height
            int increase = analogRead(switch_increase);
            int decrease = analogRead(switch_decrease);
            Serial.print(" Increase: ");
            Serial.print(increase);
            Serial.print(" Decrease: ");
            Serial.println(decrease);
            if (increase == 0) {
                tempHeight = tempHeight + 1;
                controlDesk(tempHeight);
            } else if (decrease == 0) {
                tempHeight = tempHeight - 1;
                controlDesk(tempHeight);
            } else {
                controlDesk(tempHeight);
            }
            enable = analogRead(switch_enable);
        }
    }

}

void controlDesk(int targetHeight) {
    long distance = measureDistance();
    Serial.print("Target height: ");
    Serial.print(targetHeight);
    Serial.print(" Current height: ");
    Serial.println(distance);
    // Implementing hysteresis to avoid constant minor adjustments
    if (distance < targetHeight - hysteresis) {
        raiseDesk();
    } else if (distance > targetHeight + hysteresis) {
        lowerDesk();
    } else {
        stopDesk();
    }
}

long measureDistance() {
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    long duration = pulseIn(echoPin, HIGH);
    long distance = (duration / 2) / 29.1;
    Serial.println(distance);
    return distance;
}

void raiseDesk() {
    Serial.println("Raising desk");
    digitalWrite(led, HIGH);
    digitalWrite(raisePin, HIGH);
    digitalWrite(lowerPin, LOW);
}

void lowerDesk() {
    Serial.println("Lowering desk");
    digitalWrite(led, HIGH);
    digitalWrite(raisePin, LOW);
    digitalWrite(lowerPin, HIGH);
}

void stopDesk() {
    Serial.println("Desk is stable");
    digitalWrite(led, LOW);
    digitalWrite(raisePin, LOW);
    digitalWrite(lowerPin, LOW);
}
